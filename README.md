# Getting Setup with GitLab

### Create Your Account ###

Visit **[GitLab](https://gitlab.com/users/sign_in#register-pane)** and register an account.  If you prefer to use your Notre Dame single-sign-on, you can register using **Google** and use your **netid@nd.edu** address to log in.  There is an email confirmation step, and then you should be good.

![GitLab Registration Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitlab1.jpg)

You won't be able to see the private repositories until after you have been added to the private groups.

### Using GitKraken on the Desktop ###

You *could* use standard `git` command line tools to pull and push commits to the repositories, however for consistency in our instructions we're going to assume you're downloading **[GitKraken](https://gitkraken.com/)** which is a GUI application for working with git repos.  GitKraken will nag you about Pro features the first time you use them, but since we are using it non-commercially, we can simply dismiss them.  GitKraken is one of the best Git clients that supports Windows, Mac, and various flavors of Linux.

Once you have downloaded GitKraken, it will ask you to create an account at first launch.

![GitKraken Registration Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken1.jpg)

![GitKraken Profile Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken2.jpg)

Once you have an account and verified the email, click the **GitLab** icon under the box **Start a hosted project**. It will be the **second** icon.

![GitKraken Open Repo Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken3.jpg)

Then it will ask you to connect your GitLab account.  Click **Connect to GitLab** and then authorize the app.  Once back in GitKraken, you need to click the green **Generate SSH key and add to GitLab** button.  This key allows you to read and write to the repositories.  In the upper left, click **Exit Preferences**.

![GitKraken GitLab Config Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken4.jpg)

Now go to the **File** menu and click **Clone Repo**, then in the second (middle) column select **GitLab.com**.

![GitKraken Clone Repo Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken5.jpg)

In the right-most column, click **Browse** and choose where you want to save the files locally while you work on them.  Then select the repository that you want to work on and click the **Clone the repo!** button.  At the top of the window it will offer to open the project for you once it finished pulling the repository.  (Here is where it prompts for Pro features, simply dismiss it.)

In the middle of the window you will see the history of the project, and the details of the changes to the right.  You have successfully pulled the project to your computer and can edit it with your favorite text editor.  I suggest VScode, but any editor will do.

As an alternate to pulling the project ourselves, we can use scripts and deployment tokens to automate getting the project in read-only mode.  For example, if you paste the command below in your Terminal app, you'll also be able to pull the repo, but not push any changes to it.  It will likely place it in your home directory if you simply open a new Terminal and run the command.

```sh
$ git clone https://gitlab+deploy-token-15933:DJNYpXDAENP5LQo7EwCU@gitlab.com/nd-esc/esc-nfs-client.git
```

### Saving and Pushing Changes ###

![GitKraken Staging Changes Screen](https://gitlab.com/nd-esc/esc-git-process/raw/master/img/gitkraken6.jpg)

If you make changes to the original repository, or add new files, you will need to stage those changes before they can be pushed up to the repo.  You do this by opening the project in **GitKraken** and clicking the **Stage all changes** button in the right column.  You 'll see the files move from the **Unstaged files** area to the **Staged files** area.  Near the bottom of the left column you need to add a short summary in the **Commit Message area**, then you can click the  **Stage files/changes to commit** button. 

Now the only thing left to do is click the **Push** icon at the top of the window and the changes have been pushed to the repo for others to see.

It's always nessesary when returning to a project to click the **Pull** icon first, to get any changes that other team members may have saved before you start working, and always remember to push your chnages or no one else will see them and you may need to resolve conflicts if two people change the same file.